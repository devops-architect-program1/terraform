provider "aws" {

 region = "ap-south-1"

}

resource "aws_instance" "example" {

 ami = "ami-03a933af70fa97ad2"
 instance_type = "t2.micro"

}

resource "aws_s3_bucket" "s3" {

 bucket = "wezvatech-demo-s3-july2023"

}
